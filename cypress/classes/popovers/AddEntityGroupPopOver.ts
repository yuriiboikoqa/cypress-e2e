import { DialogPopOver } from './DialogPopOver'

export class AddEntityGroupPopOver extends DialogPopOver {
  constructor(
    protected entityLabelField = '[placeholder="Enter unique entity label."]',
    protected entityDescriptionField = '[placeholder="Describe this for developers."]',
    public primaryKey = '[title="Primary Key"]',
    public dependsOn = '[title="Depends On"]',
    protected sharedEndpoint = '[title="Shared Endpoint"]',
    protected toggleElement = (element: string) =>
      cy.get(element, { timeout: 1000 }).should('be.visible').filter(':visible').parent().next().find('button').last(),
  ) {
    super()
  }

  toogle(element: string) {
    return cy
      .get(element, { timeout: 1000 })
      .should('be.visible')
      .filter(':visible')
      .parent()
      .next()
      .find('button')
      .last()
  }

  fillEnitityGroup(group: object, toogle: boolean) {
    cy.input(this.entityLabelField, group.entityGroupLabel)
    cy.input(this.entityDescriptionField, group.enitityGroupDescription)
    if (group.entityGroupPrimaryKey) {
      cy.selectCustom(this.primaryKey, group.entityGroupPrimaryKey)
    }

    cy.wait(500)
    cy.multiselect(this.dependsOn, group.dependsOn)
    if (toogle === true) {
      this.toogle(this.sharedEndpoint).click().should('have.class', 'ant-switch-checked')
    } else {
      this.toogle(this.sharedEndpoint).click().should('not.have.class', 'ant-switch-checked')
    }
  }
}

export default new AddEntityGroupPopOver()
