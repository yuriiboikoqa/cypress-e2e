/* eslint-disable import/prefer-default-export */
export const globalPatternData = {
  globalPattern: {
    name: 'NewPattern',
    type: 'Agent Assist',
    description: 'Test Description',
    channels: ['RWC', ' IVR'],
    nodeCount: '0',
    outcomes: ['HITL', 'Abandon'],
  },
  PatternUpdated: {
    name: 'TestPatternNameUpd',
    type: 'Mobile App',
    description: 'Test Description Updated',
    nodeCount: '0',
    channels: ['SMS', ' VoIP'],
    outcomes: ['Contained', 'HITL'],
  },
  GlobalToFallback: {
    name: 'GlobalPatternToFallback',
    outcomes: ['HITL'],
    type: 'Fallback',
    nodeCount: '0',
    channels: 'IVR',
    description: '',
  },
  GlobalPatternToEscalate: {
    name: 'GlobalPatternToEscalate',
    outcomes: ['HITL'],
    type: 'Escalate',
    nodeCount: '0',
    channels: 'IVR',
    description: '',
  },
  FallbackToGlobalPattern: {
    name: 'FallbackToGlobalPattern',
    outcomes: ['HITL'],
    type: 'Outbound',
    nodeCount: '0',
    channels: 'IVR',
    description: '',
  },
  EscalateToGlobalPattern: {
    name: 'EscalateToGlobalPattern',
    outcomes: ['HITL'],
    type: 'Mobile App',
    nodeCount: '0',
    channels: 'IVR',
    description: '',
  },

  patternToDelete: 'Pattern to Delete',
  pattern9522: 'TestPattern9522',
  patternToEdit: 'Pattern to Edit',
  // test data used for separated paramaters for each value. Remade as single object. Should delete
  skillPattern: 'Skill Pattern',
  patternName: 'TestPatternName',
  patternType: 'Agent Assist',
  patternDescription: 'Test Description',
  patternChannels: ['RWC', ' IVR'],
  patternOutcomes: ['Abandon', 'HITL'],
  patternNameUpd: 'TestPatternNameUpd',
  patternTypeUpd: 'Mobile App',
  patternDescriptionUpd: 'Test Description Updated',
  patternChannelsUpd: ['SMS', ' VoIP'],
  patternOutcomesUpd: ['Escalate', 'Fallback'],
  channelsContainsR: ['IVR', 'RWC', 'rSMS'],
  outcomesContainsA: ['Escalate', 'Fallback', 'Abandon', 'Contained'],

  // messages
  noPatternMsg: 'No Patterns yet added, Use (+) Add button below to create a new top-level pattern flow.',
  patternAddMsg: 'Successfully added pattern', // - not added yet
  patternUpdatedMsg: 'Successfully updated pattern',
  patternDeletedMsg: 'Successfully deleted pattern',

  notUniqueErrMsg: 'Label is not unique!',
  // tabs
  globalPatternTab: 'Global Patterns',
  escalateFallbackTab: 'Escalate-Fallback',
  // dropdown Pattern sections
  globalPatternSection: 'Global Patterns',
  escalateFallbackSection: 'Escalate-Fallback',
  skillPatternsection: 'Skills',
}
