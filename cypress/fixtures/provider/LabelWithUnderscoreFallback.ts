let labelWithUnderscoreFallback: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
}[] = [
  {
    title: '[CMTS-13994] Global Fallback outcome cannot be added when a link label contains an underscore at the end',
    preconditionId: '13994',
    linkLabel: 'ab_',
    linkType: ' Normal (down) ',
  },
  {
    title:
      '[CMTS-13993] Global Fallback outcome cannot be added when a link label contains an underscore in the middle',
    preconditionId: '13993',
    linkLabel: 'abSS_D123',
    linkType: ' Normal (down) ',
  },
]

export default labelWithUnderscoreFallback
