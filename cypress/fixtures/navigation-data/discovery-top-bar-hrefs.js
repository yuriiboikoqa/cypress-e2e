// eslint-disable-next-line import/prefer-default-export
export const DiscoveryTopBarHrefs = {
  corpus: '/discovery/corpus',
  topics: '/discovery/view',
}
