import FilterBar from '../../classes/bars/FilterBar'
import NavigationBar from '../../classes/navigation/NavigationBar'
import PalleteBox from '../../classes/PaletteBox'
import AddNewClosedQuestionNodePopUp from '../../classes/popups/AddNewOpenQuestionNodePopUp'
import NodeValidation from '../../classes/validation/NodeLabel'
import DiagramUtils from '../../support/DiagramUtils'
import NodeLabel from '../../fixtures/data/validation/validationNodeLabel'
import LinkLabel from '../../fixtures/data/validation/validationLinkLabel'
import { ProjectInfo } from '../../types/response/ProjectInfo'

before(() => {
  cy.fixture('projects-templates/add-node-project.json').as('projects')

  cy.get<ProjectInfo>('@projects').then((json: ProjectInfo) => {
    cy.loginAPI()
    cy.createNewProject(json).then((projectInfo: ProjectInfo) => {
      Cypress.env('projectId', projectInfo.id)
      cy.loginUI()
      NavigationBar.selectProject(projectInfo.label)
    })
    NavigationBar.openPatternsPage()
    FilterBar.selectPattern('skillrouter')
    DiagramUtils.findDiagram()
    DiagramUtils.addNodeInDiagram(PalleteBox.skillRouterNodeIdSelector, 'nodeLabel')
  })
})

after(() => {
  cy.removeProject(Cypress.env('projectId'))
})

describe('Add SkillRouter Node Modal Dialog Validation', { tags: '@validation' }, () => {
  NodeLabel.forEach((input) => {
    it(input.title, () => {
      NodeValidation.nodeLabel(input)
    })
  })

  LinkLabel.forEach((input) => {
    it(input.title, () => {
      NodeValidation.linkLabel(input)
    })
  })
  it('skills can not be empty', () => {
    NodeValidation.skillsEmpty()
  })

  it('input not existing skill', () => {
    NodeValidation.multiselectNotExist('Skills', 'ABC')
  })
})
