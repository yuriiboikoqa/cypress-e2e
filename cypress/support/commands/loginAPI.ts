declare global {
  namespace Cypress {
    interface Chainable {
      loginAPI: typeof loginAPI
    }
  }
}

export default function loginAPI(
  username: string = Cypress.env('username'),
  password: string = Cypress.env('password'),
): void {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username,
      password,
    },
  })
    .as('loginResponse')
    .then((response) => {
      Cypress.env('token', response.body.payload.accessToken)
    })
    .its('status')
    .should('eq', 200)
}
