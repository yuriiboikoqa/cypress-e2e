declare global {
  namespace Cypress {
    interface Chainable {
      multiselect: typeof multiselect
    }
  }
}

export default function multiselect(select: string, valueArray: Array<string>): void {
  cy.get(select)
    .filter(':visible')
    .parent()
    .next()
    .find('.ant-select-selection__rendered')
    .then(($items) => {
      if ($items.find('.ant-select-selection__choice__remove').length > 0) {
        cy.get(select)
          .filter(':visible')
          .parent()
          .next()
          .find('.ant-select-selection__choice__remove')
          .click({ multiple: true })
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').filter(':visible').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).scrollIntoView()
          cy.get('li.ant-select-dropdown-menu-item')
            .filter(':visible')
            .contains(valueArray[i])
            .should('be.visible')
            .click({ force: true })
          cy.get(select).filter(':visible').click()
        }
      } else {
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').filter(':visible').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).filter(':visible').scrollIntoView()
          cy.get('[role="listbox"]')
            .filter(':visible')
            .find('li.ant-select-dropdown-menu-item')
            .filter(':visible')
            .contains(valueArray[i])
            .should('be.visible')
            .click({ force: true })
          cy.get(select).filter(':visible').click()
        }
      }
    })
}
