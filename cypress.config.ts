/* eslint-disable */

import { defineConfig } from 'cypress'
import xlsx from 'node-xlsx'
const { Client } = require('pg')


export default defineConfig({
  projectId: 's27aar',
  reporter: 'cypress-mochawesome-reporter',
  reporterOptions: {
    charts: true,
    reportPageTitle: 'DNA Automation',
    mochaFile: './cypress/results/my-test-output-${date}.xml',
    reportFilename: '[status]_[datetime]-report',
    timestamp: 'isoDateTime', // https://github.com/felixge/node-dateformat
    embeddedScreenshots: true,
    inlineAssets: true,
    saveAllAttempts: false,
    overwrite: false,
      showPending: false,
      showSkipped: false,
  },
  e2e: {
    viewportHeight: 900,
    viewportWidth: 1600,
    videoUploadOnPasses: false,
    watchForFileChanges: false,
    specPattern: 'cypress/e2e/**',
    supportFile: 'cypress/support/e2e.js',
    setupNodeEvents(_on, config) {
      require('@cypress/grep/src/plugin')(config);



      _on('task', { parseXlsx( filePath )
        {
          const jsonData = xlsx.parse(filePath)
          //const workSheetsFromFile = xlsx.parse(`${__dirname}/myFile.xlsx`);
          console.log(jsonData)

          return jsonData
        }
      })
      require('cypress-mochawesome-reporter/plugin')(_on)
      // require('cypress-grep/src/plugin')
      _on("task", {
        async cleanTableProjects(){
          const client = new Client({
            user: "dhb_user",
            password: "IbMD9Pg3wMkkOgq13FQLtfEaF2b",
            host: "ip-qa-test2-dhb-db.ctnm1le0zg88.us-east-1.rds.amazonaws.com",
            database: "dhbdb",
            ssl: true,
            port: 5432,
          })
          const deleteProjects ="WITH projects AS (SELECT \"projectObject\"->>'label' AS label" +
              " FROM \"dhb_projects\" WHERE \"projectObject\"->>'label' LIKE '%ReferenceProject%')" +
              " DELETE FROM \"dhb_projects\" WHERE \"projectObject\"->>'label' NOT IN ( SELECT label FROM projects)"
          const truncateUserProjects = " DELETE FROM \"dhb_user_projects\" "
          await client.connect()
          client.query(truncateUserProjects)
          const resdeleteProjects = await client.query(deleteProjects)
          await client.end()
          return resdeleteProjects.rows;
        },
        async deleteUser(user){
          const client = new Client({
            user: "dhb_user",
            password: "IbMD9Pg3wMkkOgq13FQLtfEaF2b",
            host: "ip-qa-test2-dhb-db.ctnm1le0zg88.us-east-1.rds.amazonaws.com",
            database: "dhbdb",
            ssl: true,
            port: 5432,
          })
          const queryDeleteUser = "DELETE FROM dhb_users WHERE \"username\" = " + "'" + user + "'"
          await client.connect()
          const res = await client.query(queryDeleteUser)
          await client.end()
          return res.rows;
        }
      })

      _on('task', {
        specsWithTag: async ({tag}) => {
          const {globbySync} = await import('globby')
          const specFiles = globbySync(config.specPattern, {
            cwd: __dirname,
            ignore: config.excludeSpecPattern,
          })
          const regex = new RegExp(`{\\s*((tags)|(tag)):\\s*[',"]${tag}[',"]\\s*}`)
          return specFiles.filter(spec => {
            const fullPath = path.join(__dirname, spec)
            const specCode = fs.readFileSync(fullPath, { encoding: 'utf8' })
            return regex.test(specCode)
          })
        }
      })
      return config
    },
    baseUrl: 'NDA',
    env: {
      username: 'dhb-admin',
      password: 'dhb@dm!n123',
      baseUrlAPI: 'NDA',
      grepFilterSpecs: true,
      grepOmitFiltered: true,
      //grepIntegrationFolder: 'cypress/e2e',

    },


    // retries: 1,
    defaultCommandTimeout: 30000,
  },
  video: false,
  screenshotOnRunFailure: false,
  trashAssetsBeforeRuns: true,
  // cacheAcrossSpecs: true,
})
