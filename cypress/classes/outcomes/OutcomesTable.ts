export class OutcomesTable {
  constructor(
    protected leftContainerSel = '[ref="eLeftContainer"]',
    protected validationMsg = '.ant-form-explain',
    protected redHighlighted = '.ant-form-item-control.has-error',
    protected requiredField = '.ant-form-item-required',
    protected centerContainerSel = '[ref="eCenterContainer"]',
    protected outcomeRow = '[role="row"]',
    protected editBtnTableSel = '.editButton',
    protected deleteBtnTableSel = '*[class^="deleteButton"]',
    protected leftContainer = () => cy.get('[ref="eLeftContainer"]'),
    protected centerContainer = () => cy.get('[ref="eCenterContainer"]'),
    protected checkIcon = () => cy.get('[aria-label="icon :check"]'),
    protected deleteBtnTable = () => cy.get('*[class^="deleteButton"]'),
    protected projectOutcomesTable = () => cy.get('#projectedView'),
    protected projectedOutcomesTable = () => cy.get('#metricsProjected'),
    ) {}

  getRow(projectedOutcomeLbl: string) {
    this.leftContainer().contains(projectedOutcomeLbl).parent().parent().parent().attribute('row-id')

    return this
  }

  getLastRow() {
    this.leftContainer().children().last().find('b')

    return this
  }

  checkOutcomeLabelUnique() {
    return cy.get(this.validationMsg).should('have.text', 'Label is not unique!')
  }

  checkNoRowsMsg(table: string, message: string) {
    cy.get(table).find(this.noRowsMsg).should('exist').and('have.text', message)
  }

  checkDefaultValue(select: string, value: string) {
    return cy.get(select).parent().next().find('.ant-select-selection-selected-value').should('have.text', value)
  }

  getDeleteBtn(table: string, name: string) {
    return cy.get(table).find(this.outcomeRow).contains(name).next(this.deleteBtnTableSel)
  }

  getEditBtn(table: string, name: string) {
    // cy.get(table).find('[ref="eBodyViewport"]').scrollTo('bottom')
    return cy
      .get(table)
      .find(this.leftContainerSel)
      .find('[role="row"]')
      .contains(name)
      .parent()
      .children(this.editBtnTableSel)
  }
}
export default new OutcomesTable()
