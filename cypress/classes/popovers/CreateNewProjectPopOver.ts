import { DialogPopOver } from './DialogPopOver'
import { contentsProject } from '../../fixtures/data/outcomesTestData.js';

export class CreateNewProjectPopOver extends DialogPopOver {
    constructor(
     public projectNameField = () => cy.get( '[placeholder="Enter unique project label."]'),
     public projectDescriptionField = () => cy.get('[placeholder="Describe this Project."]'),
     public projectTypeSel = () => cy.get('[title="Type"]'),
     public popoverCont = () => cy.get('.ant-popover-inner-content'),
     public noticeMessage = () => cy.get('.ant-form-explain'),
     public createProjectBtn = () => cy.get('#createNewProjectButton'),
     public exportProjectBtn = () => cy.get('[data-cy="ExportProjectButton"] button'),
     public fillNameField = () => cy.get('[placeholder="Enter unique project label."]'),
    ) {
        super()
    }

    formExplainMessage(message:string){
     this.noticeMessage().should('be.visible').contains(message)
       }
      
    checkFieldDefault(){  //method for check placeholders default value
     this.projectNameField().parent().parent().parent().prev().parent().should("have.class",'ant-row ant-form-item')
     this.projectDescriptionField().parent().parent().parent().prev().parent().should("have.class",'ant-row ant-form-item')
     this.projectTypeSel().parent().parent().should("have.class",'ant-row ant-form-item')
       }

    fillProjectName(placeholder:any){
     this.fillNameField().should('be.visible').type(placeholder)
       }

  }
  //ant-popover-inner-content
  export default new CreateNewProjectPopOver()
  