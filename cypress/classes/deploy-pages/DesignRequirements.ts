export class DesignRequirements {
  constructor(
    private flowsTableRow = () => cy.get('[role="row"] [role="gridcell"]'),

    public selectedFlowTable = () => '[row-id="detail_0"] .ag-theme-alpine',

    public flowDiagram = () => cy.get('[role="presentation"] img[class="full-width-details"]'),
    public editBtnSel = (text: string) => cy.xpath(`//b[contains(text(), "${text}")]/parent::div//button[2]`),
  ) {}

  checkFlowDiagramState(state: string) {
    this.flowDiagram().should(state)

    return this
  }

  checkSelectedFlowTableState(state: string) {
    cy.get(this.selectedFlowTable()).should(state)

    return this
  }

  checkColorOfSelectedFlowRow(flow: string, color: string) {
    this.flowsTableRow()
      .contains(flow)
      .parents('[aria-label="Press SPACE to select this row."]')
      .should('contain.css', 'background', `rgb(${color}) none repeat scroll 0% 0% / auto padding-box border-box`)

    return this
  }

  clickEditBtn(text: string, opt: Partial<Cypress.ClickOptions> = {}) {
    this.editBtnSel(text).should('be.visible', text).click(opt)

    return this
  }
}
export default new DesignRequirements()
