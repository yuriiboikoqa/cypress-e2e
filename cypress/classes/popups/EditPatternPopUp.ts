export class EditPatternPopUp {
  constructor(
    protected channelFieldSel = '//div[contains(text(),"Select from available channels")]/parent::div/parent::div',
    protected appChannelSel = '//li[@class="ant-select-dropdown-menu-item" and contains(text(),"App")]',
    protected smsChannelSel = '//li[@class="ant-select-dropdown-menu-item" and contains(text(),"SMS")]',
    protected saveButtonSel = 'button[class="ant-btn ant-btn-primary"]',
    protected channelSel = (text: string) => cy.xpath(`//li[@title='${text}']`),
    protected descriptionFiledSel = 'textarea[placeholder="Describe this Project."]',
    protected deleteSmsChannel = '//li[@title="SMS"]//span',
    protected deleteIvrChannel = '//li[@title="IVR"]//span',
    protected deleteAppChannel = '//li[@title="App"]//span',
    protected deleteChannelSel = (text: string) => cy.xpath(`//li[@title="${text}"]//span`),
    protected channelAddSel = (text: string) =>
      cy.xpath(`//li[@class="ant-select-dropdown-menu-item" and contains(text(),"${text}")]`),
    protected cancelButtonSel = '//button[@class="ant-btn"]//span[contains(text(),"Cancel")]',
  ) {}

  clickChannelField(opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.channelFieldSel).should('be.visible').click(opt)

    return this
  }

  clickChannelAdd(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.channelAddSel(text).should('be.visible', text).click(opt)

    return this
  }

  clickSmsChannel(opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.smsChannelSel).should('be.visible').click(opt)

    return this
  }

  clickSaveButton(opt?: Partial<Cypress.TypeOptions>) {
    cy.get(this.saveButtonSel).should('be.visible').click(opt)

    return this
  }

  checkChannel(text: string) {
    this.channelSel(text).should('be.visible', text)

    return this
  }

  isNotExistChannel(text: string) {
    this.channelSel(text).should('not.exist', text)

    return this
  }

  clickDescriptionField(opt?: Partial<Cypress.TypeOptions>){
    cy.get(this.descriptionFiledSel).should('be.visible').click(opt)
  }

  clickDeleteChannel(text: string, opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    this.deleteChannelSel(text).should('be.visible', text).click(opt)

    return this
  }

  clickCancelButton(opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true

    cy.xpath(this.cancelButtonSel).should('be.visible').click(opt)

    return this
  }
}

export default new EditPatternPopUp()
