import { ProjectInfo } from '../../types/response/ProjectInfo'
import NavigationBar from '../../classes/navigation/NavigationBar'
import ControlPanelPage from '../../classes/pages/UsersPage'
import AddNewUserPopOver from '../../classes/popovers/AddNewUserPopOver'
import { ControlPanelTopBarHrefs } from '../../fixtures/navigation-data/control-panel-top-bar-hrefs'
import LoginPage from '../../classes/pages/LoginPage'
import UserSideBar from '../../classes/bars/UserSideBar'
import LeftNavigationBar from '../../classes/navigation/LeftNavigationBar'
import DiagramBar from '../../classes/bars/DiagramBar'
import ReEnablePopUp from '../../classes/popups/ReEnablePopUp'

describe('Create user from Control Panel', () => {
  beforeEach(() => {
    cy.restoreLocalStorage()

    cy.precondition('empty')
  })

  afterEach(() => {
    cy.get<ProjectInfo>('@current-project').then((json: ProjectInfo) => {
      cy.removeProject(json.id)
    })
  })

  let validUser: {
    title: string
    username: string
    email: string
    info: string
  }[] = [
    {
      title: '[CMTS-13668] To validate the user can be created through control panel',
      username: 'Testuser' + Date.now(),
      email: 'test' + Date.now() + '@test.com',
      info: 'User created',
    },
    {
      title: '[CMTS-14259] To validate it is possible to create the user with underscore in the username',
      username: 'Testuser_' + Date.now(),
      email: 'test' + Date.now() + 1 + '@test.com',
      info: 'User created',
    },
  ]

  validUser.forEach((input) => {
    it(input.title, { tags: '@smoke' }, () => {
      NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
      ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
      AddNewUserPopOver.isOpen()
        .setUsername(input.username)
        .setEmail(input.email)
        .setFirstName('Testname')
        .setLastName('Testlastname')
        .setPassword('12345678')
        .setRepeatPassword('12345678')
        .clickOnAddBtn()
        .isSuccessToastMessageDisplayed(input.info)
        .isClose()

      ControlPanelPage.clickOnUsernameLabel().isUserDisplayed(input.username)
    })
  })

  let invalidUserName: {
    title: string
    username: string
    error: string
  }[] = [
    {
      title: '[CMTS-14258] To validate it is NOT possible to create the user with the space in the username',
      username: 'Testuser ' + Date.now(),
      error: 'Username must be alphanumeric',
    },
    {
      title: '[CMTS-14260] To validate it is NOT possible to create the user with special characters in the username',
      username: 'Testuser#+-//' + Date.now(),
      error: 'Username must be alphanumeric',
    },
    {
      title: '[CMTS-14261] To validate it is NOT possible to create the user with the username less than 5 characters',
      username: Math.random().toString(36).slice(2, 6),
      error: 'Username must be at least 5 chars',
    },
    {
      title: '[CMTS-14262] To validate it is NOT possible to create the user with the username more than 32 characters',
      username: 'OZHBuCioHTG0jklJjktF9IITAa9bLs7bX',
      error: 'Username can be at most 32 chars',
    },
  ]

  invalidUserName.forEach((input) => {
    it(input.title, () => {
      NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
      ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
      AddNewUserPopOver.isOpen()
        .setUsername(input.username)
        .setEmail('test' + Date.now() + '@test.com')
        .setFirstName('Testname')
        .setLastName('Testlastname')
        .setPassword('12345678')
        .setRepeatPassword('12345678')
        .clickOnAddBtn()
        .isUsernameWarningMessageDisplayed(input.error)
        .isOpen()

      ControlPanelPage.isUserNotDisplayed(input.username)
    })
  })

  let invalidEmail: {
    title: string
    email: string
    error: string
  }[] = [
    {
      title: '[CMTS-14263] To validate it is NOT possible to create the user with the space in email field',
      email: 'test ' + Date.now() + '@test.com',
      error: 'Must be a valid email',
    },
    {
      title: '[CMTS-14264] To validate it is NOT possible to create the user with the underscore in email field',
      email: 'test_' + Date.now() + '@test.com',
      error: 'Must be a valid email',
    },
    {
      title:
        '[CMTS-14265] To validate it is NOT possible to create the user with the special characters in email field',
      email: 'test#<+*&|' + Date.now() + '@test.com',
      error: 'Must be a valid email',
    },
  ]

  invalidEmail.forEach((input) => {
    it(input.title, () => {
      NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
      ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
      AddNewUserPopOver.isOpen()
        .setUsername('Testuser_' + Date.now())
        .setEmail(input.email)
        .setFirstName('Testname')
        .setLastName('Testlastname')
        .setPassword('12345678')
        .setRepeatPassword('12345678')
        .clickOnAddBtn()
        .isOpen()
        .isEmailWarningMessageDisplayed('Must be a valid email')

      ControlPanelPage.isUserNotDisplayed(input.email)
    })
  })

  it('[CMTS-14266] To validate the user with already exist username can not be created', () => {
    let username = 'Testuser' + Date.now()
    let email1 = 'test' + Date.now() + '@test.com'
    let email2 = 'test1' + Date.now() + '@test.com'

    NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
    ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
    AddNewUserPopOver.isOpen()
      .setUsername(username)
      .setEmail(email1)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('12345678')
      .setRepeatPassword('12345678')
      .clickOnAddBtn()
      .isSuccessToastMessageDisplayed('User created')
      .isClose()

    ControlPanelPage.clickOnUsernameLabel().isUserDisplayed(username)

    ControlPanelPage.clickOnAddUserBtn()
    AddNewUserPopOver.setUsername(username)
      .setEmail(email2)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('12345678')
      .setRepeatPassword('12345678')
      .clickOnAddBtn()
      .isErrorToastMessageDisplayed(
        'Failed to create new user: Error while creating user User with username already exists.',
      )
      .isOpen()
  })

  it('[CMTS-14267] To validate the user with already exist email can not be created', () => {
    let username1 = 'Testuser' + Date.now()
    let username2 = 'Testuser1' + Date.now()
    let email = 'test' + Date.now() + '@test.com'

    NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
    ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
    AddNewUserPopOver.isOpen()
      .setUsername(username1)
      .setEmail(email)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('12345678')
      .setRepeatPassword('12345678')
      .clickOnAddBtn()
      .isSuccessToastMessageDisplayed('User created')
      .isClose()
    ControlPanelPage.clickOnUsernameLabel().isUserDisplayed(username1)

    ControlPanelPage.clickOnAddUserBtn()
    AddNewUserPopOver.isOpen()
      .setUsername(username2)
      .setEmail(email)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('12345678')
      .setRepeatPassword('12345678')
      .clickOnAddBtn()
      .isErrorToastMessageDisplayed(
        'Failed to create new user: Error while creating user User with email ID already exists.',
      )
      .isOpen()
  })

  let InvalidPassword: {
    title: string
    pass: string
    rpass: string
    error: string
  }[] = [
    {
      title:
        '[CMTS-14268] To validate it is NOT possible to create the user if the password and repeat password do not match',
      pass: '12345678',
      rpass: '23456781',
      error: 'Password does not match!',
    },
    {
      title:
        '[CMTS-14269] To validate it is NOT possible to create the user if the  one password field is filled in',
      pass: '12345678',
      rpass: '',
      error: 'Password is required',
    },
  ]

  InvalidPassword.forEach((input) => {
    it(input.title, () => {
      let username = 'Testuser' + Date.now()
      let email = 'test' + Date.now() + '@test.com'

      NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
      ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
      AddNewUserPopOver.isOpen()
        .setUsername(username)
        .setEmail(email)
        .setFirstName('Testname')
        .setLastName('Testlastname')
        .setPassword(input.pass)
        .setRepeatPassword(input.rpass)
        .clickOnAddBtn()
        .isRepeatPasswordWarningMessageDisplayed(input.error)
        .isOpen()

      ControlPanelPage.isUserNotDisplayed(username)
    })
  })

  it('[CMTS-14270] To validate it is NOT possible to create the user if the  repeat password field is filled in', () => {
    let username = 'Testuser' + Date.now()
    let email = 'test' + Date.now() + '@test.com'

    NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
    ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
    AddNewUserPopOver.isOpen()
      .setUsername(username)
      .setEmail(email)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('')
      .setRepeatPassword('12345678')
      .clickOnAddBtn()
      .isPasswordWarningMessageDisplayed('Password is required')
      .isRepeatPasswordWarningMessageDisplayed('Password does not match!')
      .isOpen()

    ControlPanelPage.isUserNotDisplayed(username)
  })

  it('[CMTS-14271] To validate the reset button remove all data from the fields in the Create new user pop up', () => {
    let username = 'Testuser' + Date.now()
    let email = 'test' + Date.now() + '@test.com'

    NavigationBar.clickLeftSidebarButton(ControlPanelTopBarHrefs.projects)
    ControlPanelPage.isProjectsTabOpened().openUsers().clickOnAddUserBtn()
    AddNewUserPopOver.isOpen()
      .setUsername(username)
      .setEmail(email)
      .setFirstName('Testname')
      .setLastName('Testlastname')
      .setPassword('12345678')
      .setRepeatPassword('12345678')
      .clickOnResetBtn()
      .isOpen()
      .isUsernameSet('')
      .isEmailSet('')
      .isFirstNameSet('')
      .isLastNameSet('')
      .isPasswordSet('')
      .isRepeatPasswordSet('')
  })

  it('[CMTS-14316] - User management: To validate the re enable button dissapears after click on it ', () => {
    LoginPage.open().setUsername('dhb-admin').setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnSettingBtn()
    ControlPanelPage.openUsers()
    DiagramBar.checkUserName('ab123')
    UserSideBar.clickUserManagmentSel().clickDeleteBtn().clickYesBtn()
    AddNewUserPopOver.isSuccessToastMessageDisplayed('Deleted')
    UserSideBar.clickReEnableBtn()
    ReEnablePopUp.clickYesBtn('ab123')
    AddNewUserPopOver.isSuccessToastMessageDisplayed('Enabled')
    UserSideBar.isNotExistReEnableBtn()
  })

  it('[CMTS-14318] - User management: To validate the re enable button appears after click on Delete button ', () => {
    LoginPage.open().setUsername('dhb-admin').setPassword('dhb@dm!n123').clickOnLoginButton()
    LeftNavigationBar.clickOnSettingBtn()
    ControlPanelPage.openUsers()
    DiagramBar.checkUserName('ab123')
    UserSideBar.clickUserManagmentSel().clickDeleteBtn().clickYesBtn()
    AddNewUserPopOver.isSuccessToastMessageDisplayed('Deleted')
    UserSideBar.checkEnableBtn()
  })
})
