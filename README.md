 # The structure of tests
 All tests can be found in cypress/e2e. There are two folders: API and UI.
 # Tests can't be performed  
 The environment for these tests is not valid now, so tests can't be performed. 
 # What is tested?
 Details of this project can't be told due to NDA, but it consists of diagrams - algorithms that show many ways of how conversation between users can be done. Every diagram has a flow which consists of nodes: subflow, open question (which can be answered with more than yes/no), closed question (which can be answered with yes/no) etc. 
 # Note
Each test case contains a set of actions performed on the UI, such as clicking buttons, selecting options, verifying displayed information, and interacting with various UI components. The assertions are made using Cypress commands and methods.
 # API tests
 There is only one API testing file which was chosen for example. It consists of 12 tests. The main goal of testing was to make sure that new project can be created, so as all error and validation messages are shown.
 # UI tests
 There are 6 testing files which were chosen for example. 
 #
 duplicate.to.channel.cy.ts - there are four main channels: IVR, RWC, MM and SMS. These channels can have their own flows. These tests show everything which is related to duplication of the flow to other channels, including positive and negative cases (error messages, duplication processes successful duplication).
 #
 node.duplicate.cy.ts - this testing file consists of tests which show a duplication of a single node, not the whole diagram. It also shows duplication processes inside one channel, not between many.
 #
 spec-add-skillrouter-node.cy.ts - this testing file shows the process of adding skill router node to the diagram.
 #
 spec-add-subflow-node.cy.ts - this testing file shows the process of adding subflow node to the diagram.
 # 
 subflow.nodes.cy.ts - this testing file consists of tests which show all possible operations with subflow node inside a diagram flow.
 # 
 user.create.cy.ts - this testings file consists of tests which show all processes regarding creation of a user.
 # Additional information
 All classes which are used in tests can be found at cypress/classes. All json files can be found at cypress/fixtures/project-templates.
