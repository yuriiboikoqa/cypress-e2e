let userValidCreds: { title: string; username: string; password: string }[] = [
  {
    title: '[CMTS-10970] Login page. Check login with valid data (API)',
    username: Cypress.env('username'),
    password: Cypress.env('password'),
  },
  {
    title: 'login with another username register and correct password',
    username: Cypress.env<string>('username').toUpperCase(),
    password: Cypress.env('password'),
  },
]

export default userValidCreds
