import { OutcomesTable } from './OutcomesTable'

export class GlobalInteractionPattern extends OutcomesTable {
  constructor(
    protected patternOutcomesRow = '[col-id="outcomes"]',
    protected patternTypeRow = '[col-id="patternType"]',
    protected patternChannelsRow = '[col-id="channels"]',
    protected patternNodesRow = '[col-id="nodeCount"]',
    protected patternDescriptionRow = '[col-id="description"]',
    protected createNewGlobalPatternBtn = () => cy.get('#addUseCaseButton'),
    protected globalPatternTable = () => cy.get('#useCasesView'),
    protected activeTab = () => cy.get('.useCasesGrid').filter(':visible'),
    protected globalPatternRow = () => this.globalPatternTable().find(this.centerContainerSel).find(this.outcomeRow),
    protected patternNameSel = (text: string) => cy.xpath(`//b[contains(text(),"${text}")]`),
    protected channelListSel = (text: string) => cy.xpath(`//div[text()='${text}']`),
  ) {
    super()
  }

  openCreateNewGlobalPattern(opt?: Partial<Cypress.ClickOptions>) {
    this.createNewGlobalPatternBtn().should('be.visible').click(opt)

    return this
  }

  deleteRow(name: string) {
    this.globalPatternTable().find(this.outcomeRow).contains(name).next(this.deleteBtnTableSel).click()

    return this
  }

  checkPatternAbsence(name: string) {
    this.activeTab().find(this.outcomeRow).filter(':visible').contains(name).should('not.exist')

    return this
  }

  checkPatternName(text: string){
    this.patternNameSel(text).should('have.text', text)

    return this
  }

  checkChannelnList(text: string) {
    this.channelListSel(text).should('exist', text)

    return this
  }

  isNotExistChannelnList(text: string) {
    this.channelListSel(text).should('not.exist', text)

    return this
  }

  editRow(name: string) {
    this.globalPatternTable()
      .find(this.leftContainerSel)
      .find(this.outcomeRow)
      .contains(name)
      .parent()
      .children(this.editBtnTableSel)
      .click()

    return this
  }

  chooseTab(name: string) {
    cy.get('[role="tab"]').contains(name).click()

    return this
  }

  checkNodeCount(table: string, name: string, count: string) {
    cy.get(table)
      .find(this.leftContainerSel)
      .contains(name)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(table)
          .find(this.centerContainerSel)
          .find(this.outcomeRow)
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find(this.patternNodesRow).text()).to.equal(count)
          })
      })
  }

  checkPatternFields(
    patternName: string,
    patternType: string,
    patternOutcomes: string,
    patternChannels: string,
    patternDescription: string,
  ) {
    this.activeTab()
      .find(this.leftContainerSel)
      .contains(patternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.activeTab()
          .find(this.centerContainerSel)
          .filter(':visible')
          .find(this.outcomeRow)
          .filter(':visible')
          .eq(rowNumber)
          .then(($trow) => {
            const patternTypeFirstWord = patternType.split(' ')[0]
            // const patternOutcomesStr = patternOutcomes.toString()
            const patternChannelsStr = patternChannels.toString()
            const patternOutcomesAddSpace = patternOutcomes.join(', ')
            expect($trow.find(this.patternOutcomesRow).text().trim()).to.equal(patternOutcomesAddSpace)
            expect($trow.find(this.patternTypeRow).text()).to.equal(patternTypeFirstWord)
            expect($trow.find(this.patternChannelsRow).text()).to.equal(patternChannelsStr)
            expect($trow.find(this.patternDescriptionRow).text()).to.equal(patternDescription)
          })
      })

    return this
  }

  checkGlobalPatternRow(pattern: object) {
    this.globalPatternTable()
      .find(this.leftContainerSel)
      .filter(':visible')
      .contains(pattern.name)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.globalPatternTable()
          .find(this.centerContainerSel)
          .filter(':visible')
          .find(this.outcomeRow)
          // .filter(':visible')
          .eq(rowNumber)
          .scrollIntoView()
          .then(($trow) => {
            const patternTypeFirstWord = pattern.type.split(' ')[0]
            // const patternOutcomesStr = patternOutcomes.toString()
            const patternChannelsStr = pattern.channels.toString()
            const patternOutcomesAddSpace = pattern.outcomes.join(', ')
            expect($trow.find(this.patternOutcomesRow).text().trim()).to.equal(patternOutcomesAddSpace)
            expect($trow.find(this.patternTypeRow).text()).to.equal(patternTypeFirstWord)
            expect($trow.find(this.patternChannelsRow).text()).to.equal(patternChannelsStr)
            expect($trow.find(this.patternNodesRow).text()).to.equal(pattern.nodeCount)
            expect($trow.find(this.patternDescriptionRow).text()).to.equal(pattern.description)
          })
      })

    return this
  }
}
export default new GlobalInteractionPattern()
