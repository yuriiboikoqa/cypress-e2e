import { DialogPopUp } from './DialogPopUp'

export class AddNewOutcome extends DialogPopUp {
  constructor(
    protected linkLabelSel = () => cy.xpath('//input[@placeholder="Enter unique Endpoint Label."]'),
    protected outcomeLinkType = () =>
      cy.get('div.ant-modal-wrap span.ant-form-item-children div.ant-select-selection.ant-select-selection--single'),
    protected listOutcomeLinkType = () => cy.get('ul[role="listbox"] li.ant-select-dropdown-menu-item'),
    protected linkTypeSel = () => cy.get('div.ant-select-selection__rendered'),
    protected reasonFieldSel = () => cy.get('textarea[placeholder="Describe the reason."]'),
    protected errorMessage = () => cy.get('div.ant-form-explain'),
  ) {
    super()
  }

  clickOnAddBtn(opt: Partial<Cypress.ClickOptions> = {}) {
    opt.force = true

    this.addBtnSel().click(opt)

    return this
  }

  setLinkLabel(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.linkLabelSel().sendKeys(text, opt)

    return this
  }

  clickOnLinkType(text: string, opt?: Partial<Cypress.ClickOptions>) {
    this.outcomeLinkType().click(opt)
    this.listOutcomeLinkType()
      .filter(':visible')
      .filter(':contains("' + text + '")')
      .click(opt)

    return this
  }

  isLinkTypeSet(text: string) {
    this.linkTypeSel().should('contain.text', text)

    return this
  }

  setReasonField(text: string, opt?: Partial<Cypress.TypeOptions>) {
    this.reasonFieldSel().sendKeys(text, opt)

    return this
  }

  checkReasonField(text: string) {
    this.reasonFieldSel().should('have.attr', 'placeholder', text)

    return this
  }

  checkErrorMessage(text: string) {
    this.errorMessage().should('contain.text', text)

    return this
  }
}

export default new AddNewOutcome()
