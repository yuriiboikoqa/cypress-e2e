// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import 'cypress-localstorage-commands'
import HeaderToolbar from '../classes/HeaderToolbar.js'
import LoginPage from '../classes/LoginPage.js'
import NodePropertiesPanel from '../classes/NodePropertiesPanel.js'

Cypress.Commands.add('UILogin', (username, password) => {
  let login = new LoginPage()
  cy.window().then(($win) => {
    $win.localStorage.clear()
  })
  cy.visit('/')
  login.userInput().type(username, { log: false })
  login.passwordInput().type(password, { log: false })
  login.logInButton().click()
  cy.url().should('contain', '/metrics')
})

Cypress.Commands.add('getToken', () => {
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/auth/login',
    body: {
      username: Cypress.env('username'),
      password: Cypress.env('password'),
    },
  })
    .as('loginResponse')
    .then((response) => {
      Cypress.env('token', response.body.payload.accessToken)
    })
    .its('status')
    .should('eq', 200)
  // cy.getCookie('cypress-session-cookie').should('exist')
  // cy.getCookie('connect.sid').should('exist')
  // cy.setCookie('connect.sid', 'this.connect.sid')
  // cy.visit('/designer/metrics')
})

// selects a wanted project to work with
Cypress.Commands.add('selectProject', (projectName) => {
  cy.wait(1000)
  let headerToolBar = new HeaderToolbar()
  headerToolBar.projectMenuButton().should('be.visible').click()
  cy.get('li').contains(projectName).scrollIntoView().should('be.visible').click()
  headerToolBar.projectMenuButton().should('be.visible')
})

// selects a wanted pattern on Pattens page
Cypress.Commands.add('selectPattern', (patternName) => {
  cy.wait(3000)
  cy.get('#patternFilter').should('be.visible').click()
  cy.get('.ant-select-dropdown').should('not.be.undefined')
  cy.get('.ant-select-dropdown').should('be.visible')
  cy.get('li').find('span').contains(patternName).click()
})

// closes properties popup rail
Cypress.Commands.add('closePropertiesRail', () => {
  let nodePropsPanel = new NodePropertiesPanel()
  nodePropsPanel.closePropertiesButton().should('be.visible')
  nodePropsPanel.closePropertiesButton().click()
  nodePropsPanel.closePropertiesButton().should('not.be.visible')
})

Cypress.Commands.add('beforeTest', (projectPath) => {
  cy.getToken()
  cy.fixture(projectPath).then(($json) => {
    cy.createProjectNew($json).then(($projectInfo) => {
      cy.UILogin(Cypress.env('username'), Cypress.env('password'))
      cy.selectProject($projectInfo.label)
      cy.get('div').contains($projectInfo.label).should('be.visible')
      cy.get('.topLink').contains('Patterns').should('be.visible').click()
      cy.url().should('contain', '/patterns')
      cy.selectPattern('ResetTag')
      Cypress.env('projectName', $projectInfo.label)
      Cypress.env('projectId', $projectInfo.id)
    })
  })
})

Cypress.Commands.add('createProjectNew', (projectJson) => {
  let project = projectJson
  project.label = project.label + '_' + Date.now()
  cy.request({
    method: 'POST',
    url: Cypress.env('baseUrlAPI') + '/api/projects/import/',
    headers: {
      Authorization: 'Bearer ' + Cypress.env('token'),
    },
    body: project,
  }).then((resp) => {
    expect(resp.status).equals(201)
    let obj = {
      id: resp.body.projectObject.id,
      label: resp.body.projectObject.label,
    }
    cy.wrap(obj).then(($obj) => {
      $obj
    })
  })
})

Cypress.Commands.add('deleteProjectNew', () => {
  cy.request({
    method: 'DELETE',
    url: Cypress.env('baseUrlAPI') + '/api/projects/' + Cypress.env('projectId'),
    headers: {
      Authorization: 'Bearer ' + Cypress.env('token'),
    },
  }).then((resp) => {
    expect(resp.body.success).equals(true)
  })
})

Cypress.Commands.add('checkToastMsg', (message) => {
  cy.get('.ant-message-success').should('exist').and('have.text', message)
  cy.get('.ant-message-success', { timeout: 12000 }).should('not.exist')
})

Cypress.Commands.add('getEditButtonOnTable', (table, label) => {
  return cy
    .get(table)
    .find('[role="row"]')
    .filter(':visible')
    .contains(label)
    .parent()
    .children('.editButton')
    .should('be.visible')
})

Cypress.Commands.add('getDeleteButtonOnTable', (table, label) => {
  return cy.get(table).find('[role="row"]').contains(label).next('.deleteButton')
})

Cypress.Commands.add('selectCustom', (selectName, optionValue) => {
  cy.get(selectName).parent().next().find('[role="combobox"]').filter(':visible').click()
  cy.get(selectName)
    .filter(':visible')
    .parent()
    .next()
    .find('[role="combobox"]')
    .then(($combobox) => {
      cy.wrap($combobox)
        .attribute('aria-controls')
        .then(($id) => {
          cy.get('#' + $id)
            .find('ul>li')
            .should('be.visible')
            .contains(optionValue)
            .click()
        })
    })
})

Cypress.Commands.add('multiselect', (select, valueArray) => {
  cy.get(select)
    .filter(':visible')
    .parent()
    .next()
    .find('.ant-select-selection__rendered')
    .then(($items) => {
      if ($items.find('.ant-select-selection__choice__remove').length > 0) {
        cy.get(select)
          .filter(':visible')
          .parent()
          .next()
          .find('.ant-select-selection__choice__remove')
          .click({ multiple: true })
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').filter(':visible').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).scrollIntoView()
          cy.get('li.ant-select-dropdown-menu-item')
            .filter(':visible')
            .contains(valueArray[i])
            .should('be.visible')
            .click({ force: true })
          cy.get(select).filter(':visible').click()
        }
      } else {
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').filter(':visible').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).filter(':visible').scrollIntoView()
          cy.get('[role="listbox"]')
            .filter(':visible')
            .find('li.ant-select-dropdown-menu-item')
            .filter(':visible')
            .contains(valueArray[i])
            .should('be.visible')
            .click({ force: true })
          cy.get(select).filter(':visible').click()
        }
      }
    })
})

Cypress.Commands.add('multiselectNew', (select, valueArray) => {
  cy.get(select)
    .filter(':visible')
    .parent()
    .next()
    .find('.ant-select-selection__rendered')
    .then(($items) => {
      if ($items.find('.ant-select-selection__choice__remove').length > 0) {
        cy.get(select)
          .filter(':visible')
          .parent()
          .next()
          .find('.ant-select-selection__choice__remove')
          .click({ multiple: true })
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).scrollIntoView()
          cy.get(select)
            .filter(':visible')
            .parent()
            .next()
            .find('[role="combobox"]')
            .then(($combobox) => {
              cy.wrap($combobox)
                .attribute('aria-controls')
                .then(($id) => {
                  cy.get('#' + $id)
                    .find('ul>li')
                    .contains(valueArray[i])
                    .should('be.visible')
                    .click()
                })
            })
          cy.get('li.ant-select-dropdown-menu-item')
            .filter(':visible')
            .contains(valueArray[i])
            .should('be.visible')
            .click({ force: true })
          cy.get(select).filter(':visible').click()
        }
      } else {
        for (let i = 0; i < valueArray.length; i++) {
          cy.get(select).filter(':visible').parent().next().find('[role="combobox"]').click()
          cy.get('[role="listbox"]').filter(':visible').contains(valueArray[i]).filter(':visible').scrollIntoView()
          cy.get(select)
            .filter(':visible')
            .parent()
            .next()
            .find('[role="combobox"]')
            .then(($combobox) => {
              cy.wrap($combobox)
                .attribute('aria-controls')
                .then(($id) => {
                  cy.get('#' + $id)
                    .find('ul>li')
                    .contains(valueArray[i])
                    .should('be.visible')
                    .click()
                })
            })
          cy.get(select).filter(':visible').click()
        }
      }
    })
})

Cypress.Commands.add('checkValidationField', (label, message, field, input, flag) => {
  if (input.length > 0) {
    cy.get(field).filter(':visible').clear({ force: true }).type(input)
    cy.get('.ant-popover-title').filter(':visible').click()
  } else {
    cy.get(field).filter(':visible').clear()
    cy.get('.ant-popover-title').filter(':visible').click()
  }
  if (flag == true) {
    cy.get(label)
      .filter(':visible')
      .parent()
      .siblings()
      .find('.ant-form-explain')
      .should('exist')
      .and('have.text', message)
    cy.get(label).filter(':visible').parent().siblings().find('.ant-form-item-control.has-error').should('exist')
  } else {
    cy.get(label).filter(':visible').parent().siblings().find('.ant-form-explain').should('not.exist')
    cy.get(label).filter(':visible').parent().siblings().find('.ant-form-item-control.has-error').should('not.exist')
  }
})

Cypress.Commands.add('input', (field, value) => {
  cy.get(field).filter(':visible').clear().type(value)
})

Cypress.Commands.add('parseXlsx', (inputFile) => {
  return cy.task('parseXlsx', { filePath: inputFile })
})

// commands to review

Cypress.Commands.add('simulateWheel', (wheelDelta, wheelDeltaX, wheelDeltaY) => {
  let dataTransfer = new DataTransfer()
  cy.get('#goDiagram')
    .find('canvas')
    .then(($canvas) => {
      cy.wrap($canvas).trigger('wheel', {
        wheelDelta,
        wheelDeltaX,
        wheelDeltaY,
        WheelEvent: {
          dataTransfer,
        },
      })
    })
})

Cypress.Commands.add('zoomOut', (zoomlevel) => {
  let dataTransfer = new DataTransfer()
  cy.get('#views').then(($canvas) => {
    for (let i = 0; i <= zoomlevel; i++) {
      cy.wrap($canvas).trigger('wheel', {
        wheelDelta: 0,
        wheelDeltaX: 0,
        wheelDeltaY: -300,
        WheelEvent: {
          dataTransfer,
        },
      })
    }
  })
})

Cypress.Commands.add('mouseOver', (element) => {
  let dataTransfer = new DataTransfer()
  element.trigger('mouseover', {
    MouseEvent: {
      dataTransfer,
    },
  })
})

Cypress.Commands.add('deleteProject', (url, username, password, projectId) => {
  cy.request({
    method: 'POST',
    url: url + '/auth/login',
    body: {
      username,
      password,
    },
  })
    .its('body')
    .then((body) => {
      cy.request({
        method: 'DELETE',
        url: url + '/api/projects/' + projectId,
        headers: {
          Authorization: 'Bearer ' + body.payload.accessToken,
        },
      }).then((resp) => {
        expect(resp.body.success).equals(true)
      })
    })
})

Cypress.Commands.add('createProject', (url, username, password, projectJson) => {
  return cy
    .request({
      method: 'POST',
      url: url + '/auth/login',
      body: {
        username,
        password,
      },
    })
    .its('body')
    .then((body) => {
      let project = projectJson
      project.label = project.label + '_' + Date.now()
      cy.request({
        method: 'POST',
        url: url + '/api/projects/import',
        headers: {
          Authorization: 'Bearer ' + body.payload.accessToken,
        },
        body: project,
      }).then((resp) => {
        expect(resp.status).equals(201)
        let obj = {
          id: resp.body.projectObject.id,
          label: resp.body.projectObject.label,
        }
        cy.wrap(obj).then(($obj) => {
          $obj
        })
      })
    })
})
