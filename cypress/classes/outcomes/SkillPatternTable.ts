import { OutcomesTable } from './OutcomesTable'

export class SkillPatternTable extends OutcomesTable {
  constructor(
    protected createSkillOutcomeBtn = () => cy.get('#addSkillOutcomeButton'),
    protected createSkillPatternBtn = () => cy.get('#addSkillButton'),
    protected skillPatternTable = () => cy.get('#skillsGridWrapper'),
    protected skillOutcomeTable = () => cy.get('#skillsOutcomeGridWrapper'),
    protected nodesCount = () => cy.get('[col-id="nodeCount"]'),
    protected skillOutcomeName = '.skillLabel',
    protected skillDescription = '.skillDescription',
    protected patternChannelsRow = '[col-id="channels"]',
  ) {
    super()
  }

  openCreateSkillPattern() {
    this.createSkillPatternBtn().should('be.visble').click()

    return this
  }

  openCreateSkillOutcome() {
    this.createSkillOutcomeBtn().should('be.visble').click()

    return this
  }

  deletePattern(name: string) {
    this.skillPatternTable().find(this.outcomeRow).contains(name).next(this.deleteBtnTableSel)

    return this
  }

  deleteSkill(name: string) {
    this.skillOutcomeTable().find(this.outcomeRow).contains(name).next(this.deleteBtnTableSel)

    return this
  }

  checkSkillPatternAbsence(name: string) {
    this.skillPatternTable().find(this.outcomeRow).contains(name).should('not.exist')

    return this
  }

  checkSkillOutcomeAbsence(name: string) {
    this.skillOutcomeTable().find(this.outcomeRow).contains(name).should('not.exist')

    return this
  }

  editSkillPattern(name: string) {
    this.skillPatternTable()
      .find(this.leftContainerSel)
      .find(this.outcomeRow)
      .contains(name)
      .parent()
      .children(this.editBtnTableSel)

    return this
  }

  checkSkillPatternTable(pattern: object) {
    this.skillPatternTable()
      .find(this.leftContainerSel)
      .contains(pattern.skillPatternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.skillPatternTable()
          .find(this.centerContainerSel)
          .find(this.outcomeRow)
          .eq(rowNumber)
          .then(($trow) => {
            cy.wrap($trow).click()
            const skillPatternChannelsStr = pattern.skillPatternChannels.toString()
            expect($trow.find(this.patternChannelsRow).text()).to.equal(skillPatternChannelsStr)
            // this.skillOutcomeName().should('have.text', pattern.skillPatternName)
            // this.nodesCount().should('have.text', pattern.nodeCount)
            // this.skillDescription().should('have.text', pattern.skillPatternDescription)
          })
      })

    return this
  }

 /*  checkSkillPatternTable(skillPatternName: string, skillPatternChannels: string, skillPatternDescription: string) {
    this.skillPatternTable()
      .find(this.leftContainerSel)
      .contains(skillPatternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get(this.skillPatternTable)
          .find(this.centerContainerSel)
          .find(this.outcomeRow)
          .eq(rowNumber)
          .then(($trow) => {
            cy.wrap($trow).click()
            const skillPatternChannelsStr = skillPatternChannels.toString()
            expect($trow.find(this.patternChannelsRow).text()).to.equal(skillPatternChannelsStr)

            this.skillOutcomeName().should('have.text', skillPatternName)
            this.skillDescription().should('have.text', skillPatternDescription)
          })
      })

    return this
  } */

  checkSkillOutcomesNames(skillOutcomesNames: string) {
    this.skillPatternTable()
      .find(this.leftContainerSel)
      .contains(contentsOutcomes.skillPatternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.skillPatternTable()
          .find(this.centerContainerSel)
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find('[col-id="outcomes"]').text()).to.contain(skillOutcomesNames)
          })
      })
  }

  checkSkillOutcomeFields(outcomeName: string, outcomeDescription: string, outcomeValue: string, outcomeColor: string) {
    this.skillOutcomeTable()
      .find(this.leftContainerSel)
      .contains(outcomeName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        this.centerContainer()
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find('[col-id="outcomeValue"]').text()).to.equal(outcomeValue)
            expect($trow.find('[col-id="color"]').text()).to.equal(outcomeColor)
            expect($trow.find('[col-id="description"]').text()).to.equal(outcomeDescription)
          })
      })

    return this
  }
}
export default new SkillPatternTable()
