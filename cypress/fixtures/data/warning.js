// eslint-disable-next-line import/prefer-default-export
export const warnings = {
  nodeMustHaveoneExit: 'ALL nodes must have at least one exit flow.',
  flowNotFound: 'Could not find any selected flow.',
}
