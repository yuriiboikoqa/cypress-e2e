import LoginPage from '../../classes/pages/LoginPage'

declare global {
  namespace Cypress {
    interface Chainable {
      loginUI: typeof loginUI
    }
  }
}

export default function loginUI(username?: string, password?: string): void {
  cy.clearLocalStorage()
  if (!username && !password) {
    LoginPage.open().setUsername(Cypress.env('username')).setPassword(Cypress.env('password')).clickOnLoginButton()
  } else {
    LoginPage.open().setUsername(username).setPassword(password).clickOnLoginButton()
  }
  cy.url().should('contain', '/metrics')
}
