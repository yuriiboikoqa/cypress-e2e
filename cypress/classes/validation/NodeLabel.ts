import { AddNewNodePopUp } from '../popups/AddNewNodePopUp'

class NodeValidation extends AddNewNodePopUp {
  constructor(
    protected multiSelect = (name: string) =>
      cy
        .get('[title="' + name + '"]')
        .parent()
        .parent()
        .find('.ant-select-search__field'),
    protected multiSelectError = (name: string) =>
      cy
        .get('[title="' + name + '"]')
        .parent()
        .parent()
        .find('.ant-form-explain'),
  ) {
    super()
  }

  nodeLabel(item: object) {
    if (!item.error) {
      this.labelSel().clear().type(item.input).blur().parent().parent().find('.ant-form-explain').should('not.exist')
    } else {
      this.labelSel()
        .clear()
        .then((field) => {
          if (item.input !== '') cy.wrap(field).type(item.input)
        })
        .blur()
        .parent()
        .parent()
        .find('.ant-form-explain')
        .should('contain', item.message)
    }
  }

  linkLabel(item: object) {
    if (!item.error) {
      this.linkSel().clear().type(item.input).blur().parent().parent().find('.ant-form-explain').should('not.exist')
    } else {
      this.linkSel()
        .clear()
        .then((field) => {
          if (item.input !== '') cy.wrap(field).type(item.input)
        })
        .blur()
        .parent()
        .parent()
        .find('.ant-form-explain')
        .should('contain', item.message)
    }
  }

  skillsEmpty() {
    this.multiSelect('Skills').focus().blur()
    this.multiSelectError('Skills').should('contain', 'Must have at least 1 skill')
  }

  channelsEmpty() {
    cy.get('.ant-select-selection__choice__remove').then(($remove) => {
      for (let i = 0; i < $remove.length; i++) {
        cy.get('.ant-select-selection__choice__remove').then(($remove) => {
          $remove.click()
          cy.wait(500)
        })
      }
    })
    cy.get('strong').contains('Warning').should('exist').should('be.visible')
    cy.get('p')
      .contains(' Deleting channels from a subflow may cause automatic bot deployments to misbehave. ')
      .should('exist')
      .should('be.visible')
    this.multiSelectError('Channels').should('contain', 'Must have at least 1 channel')
  }

  multiselectNotExist(multiSelect: string, input: string) {
    this.multiSelect(multiSelect).type(input)
    cy.get('p').contains('No Data').should('exist').should('be.visible')
  }
}

export default new NodeValidation()
