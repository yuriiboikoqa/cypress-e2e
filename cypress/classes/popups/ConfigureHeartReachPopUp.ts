export class ConfigureHeartReachPopUp {
  constructor(
    protected tokenField = '//label[@title="Token"]/ancestor::div[contains(@class,"ant-row ant-form-item")]//input[@class]',
    protected accountField = '//label[@title="Account"]/ancestor::div[contains(@class,"ant-row ant-form-item")]//input[@class]',
    protected pdeUrlField = '//label[@title="PDE URL"]/ancestor::div[contains(@class,"ant-row ant-form-item")]//input[@class]',
    protected addBtnSel = '//button[@class="ant-btn ant-btn-primary"]//span[contains(text(),"Add")]',
    protected closeBtnSel = '//div[contains(text(), "HeartReach")]/parent::div//button[2]',
    protected checkErrorMessageSel = (text: string) =>
      cy.xpath(`//div[@class="ant-form-explain" and contains(text(),"${text}")]`),
    protected checkErrorMessageInvalidToken = (text: string) => `//li[contains(text(), "${text}")]`,
  ) {
  }

  setPdeUrlField(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.pdeUrlField).should('be.visible').sendKeys(text, {delay: 0})
    return this
  }

  setTokenField(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.tokenField).should('be.visible').sendKeys(text, {delay: 0})

    return this
  }

  setAccountField(text: string, opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.accountField).should('be.visible').sendKeys(text, {delay: 0})

    return this
  }

  clickAddBtn(opt: Partial<Cypress.ClickOptions> = {}) {
    opt.force = true

    cy.xpath(this.addBtnSel).should('be.visible').click(opt)

    return this
  }

  clickCloseBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.closeBtnSel).should('be.visible').click(opt)

    return this
  }

  checkPdeUrlField() {
    cy.xpath(this.pdeUrlField).should('be.visible')

    return this
  }

  checkTokenField() {
    cy.xpath(this.tokenField).should('be.visible')

    return this
  }

  checkAccountField() {
    cy.xpath(this.accountField).should('be.visible')

    return this
  }

  clickTokenField(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.tokenField).should('be.visible').click(opt)

    return this
  }

  clickAccountField(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.accountField).should('be.visible').click(opt)

    return this
  }

  checkAddBtn() {
    cy.xpath(this.addBtnSel).should('be.visible')

    return this
  }

  checkCloseBtn() {
    cy.xpath(this.closeBtnSel).should('be.visible')

    return this
  }

  checkErrorMessage(text: string) {
    this.checkErrorMessageSel(text).should('contain.text', text)
  }

  checkColorAccount(text: string) {
    cy.xpath(this.accountField).should('have.css', 'border-color', text)

    return this
  }

  checkColorPde(text: string) {
    cy.xpath(this.pdeUrlField).should('have.css', 'border-color', text)

    return this
  }

  checkColorToken(text: string) {
    cy.xpath(this.tokenField).should('have.css', 'border-color', text)

    return this
  }

  checkMessageInvalidToken(text: string) {
    cy.xpath(this.checkErrorMessageInvalidToken(text)).should('contain.text', text)
  }
}

export default new ConfigureHeartReachPopUp()
