// eslint-disable-next-line import/prefer-default-export
export const messages = {
  node: {
    skillExist: 'This skill has already been added to this Router.',
  },
  characterLength: 'Length should be 2 to 32 chars',
  spacesAndunderscores: 'Label must not contain spaces or underscores.',
  uniqueName: 'Each Node Label must be uniquely named.',
  existingLabel: 'Label is not unique!',

  labelLength: 'Length should be 2 to 32 chars',
  spacesAndUnderscores: 'Label must not contain spaces or underscores.',
  startWithLetter: 'Label must start with a letter and must not contain special characters',
  existingNodeLabel: 'Label is not unique!',
  existingLinkLabel: 'Link label is not unique!',
  uniqueNodeLabel: 'Each Node Label must be uniquely named.',
  uniqueLinkLabel: 'Each Link Label must be uniquely named.',

  uniqueOutputLabel: 'Label must be unique.',

  percentageRequired: 'percentage is required',
  slashesPresent: "Please don't use slashes.",
  channelsRequired: 'channels is required',
  outcomesRequired: 'outcomes is required',
}
