declare global {
  namespace Cypress {
    interface Chainable {
      checkToastMessage: typeof checkToastMessage
    }
  }
}

export default function checkToastMessage(message: string): void {
  cy.get('.ant-message-notice').should('exist').and('have.text', message)
  cy.get('.ant-message-notice', { timeout: 12000 }).should('not.exist')
}
