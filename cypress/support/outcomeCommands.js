import { OutcomesPageElements } from '../classes/outcomesPage'
import ProjectsPageElements from '../classes/projectSelectors'

Cypress.Commands.add(
  'fillOutcome',
  (projectedOutcomeLabel, outcomeDescription, outcomePercentage, outcomeValue, outcomeColor) => {
    cy.get(OutcomesPageElements.outcomeNameField).filter(':visible').clear().type(projectedOutcomeLabel)
    if (outcomeDescription.length > 0) {
      cy.get(OutcomesPageElements.outcomeDescriptionField).filter(':visible').clear().type(outcomeDescription)
    }
    cy.get(OutcomesPageElements.outcomePercentage).filter(':visible').clear().type(outcomePercentage)
    cy.selectCustom(OutcomesPageElements.outcomeValueSel, outcomeValue)
    cy.selectCustom(OutcomesPageElements.outcomeColorSel, outcomeColor)
  },
)

Cypress.Commands.add('fillProject', (projectName, projectDescription, projectType) => {
  cy.get(ProjectsPageElements.createProjectBtn).click()
  cy.get(ProjectsPageElements.projectNameField).should('be.visible').type(projectName)
  cy.get(ProjectsPageElements.projectDescriptionField).type(projectDescription)
  cy.selectCustom(ProjectsPageElements.projectTypeSel, projectType)
})

Cypress.Commands.add('checkOutcomeFields', (projectedOutcomeUpdated, outcomePercentage, outcomeValueNeutral, outcomeColor, outcomeDescription) => {
    cy.get('#projectedView')
      .find(OutcomesPageElements.leftContainer)
      .contains(projectedOutcomeUpdated)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get('[ref="eCenterContainer"]')
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            expect($trow.find('[col-id="percentage"]').text()).to.equal(outcomePercentage)
            expect($trow.find('[col-id="outcomeValue"]').text()).to.equal(outcomeValueNeutral)
            expect($trow.find('[col-id="color"]').text()).to.equal(outcomeColor)
            expect($trow.find('[col-id="description"]').text()).to.equal(outcomeDescription)
          })
      })
  },
)

Cypress.Commands.add(
  'checkPatternFields',
  (patternName, patternType, patternOutcomes, patternChannels, patternDescription) => {
    cy.get('#useCasesView')
      .find(OutcomesPageElements.leftContainer)
      .contains(patternName)
      .parent()
      .parent()
      .parent()
      .attribute('row-id')
      .then(($attr) => {
        const rowNumber = $attr
        cy.get('#useCasesView')
          .find('[ref="eCenterContainer"]')
          .find('[role="row"]')
          .eq(rowNumber)
          .then(($trow) => {
            const patternTypeFirstWord = patternType.split(' ')[0]
            // const patternOutcomesStr = patternOutcomes.toString()
            const patternChannelsStr = patternChannels.toString()
            const patternOutcomesAddSpace = patternOutcomes.join(', ')

            expect($trow.find(OutcomesPageElements.patternOutcomesRow).text().trim()).to.equal(patternOutcomesAddSpace)
            expect($trow.find(OutcomesPageElements.patternTypeRow).text()).to.equal(patternTypeFirstWord)
            expect($trow.find(OutcomesPageElements.patternChannelsRow).text()).to.equal(patternChannelsStr)
            expect($trow.find(OutcomesPageElements.patternDescriptionRow).text()).to.equal(patternDescription)
          })
      })
  },
)

Cypress.Commands.add('checkSkillPatternTable', (skillPatternName, skillPatternChannels, skillPatternDescription) => {
  cy.get(OutcomesPageElements.skillPatternTable)
    .find(OutcomesPageElements.leftContainer)
    .contains(skillPatternName)
    .parent()
    .parent()
    .parent()
    .attribute('row-id')
    .then(($attr) => {
      const rowNumber = $attr
      cy.get(OutcomesPageElements.skillPatternTable)
        .find('[ref="eCenterContainer"]')
        .find('[role="row"]')
        .eq(rowNumber)
        .then(($trow) => {
          cy.wrap($trow).click()
          const skillPatternChannelsStr = skillPatternChannels.toString()
          expect($trow.find(OutcomesPageElements.patternChannelsRow).text()).to.equal(skillPatternChannelsStr)

          cy.get(OutcomesPageElements.skillOutcomeName).should('have.text', skillPatternName)
          cy.get(OutcomesPageElements.skillDescription).should('have.text', skillPatternDescription)
        })
    })
})

Cypress.Commands.add('checkSkillOutcomeFields', (outcomeName, outcomeDescription, outcomeValue, outcomeColor) => {
  cy.get(OutcomesPageElements.skillOutcomeTable)
    .find(OutcomesPageElements.leftContainer)
    .contains(outcomeName)
    .parent()
    .parent()
    .parent()
    .attribute('row-id')
    .then(($attr) => {
      const rowNumber = $attr
      cy.get('[ref="eCenterContainer"]')
        .find('[role="row"]')
        .eq(rowNumber)
        .then(($trow) => {
          expect($trow.find('[col-id="outcomeValue"]').text()).to.equal(outcomeValue)
          expect($trow.find('[col-id="color"]').text()).to.equal(outcomeColor)
          expect($trow.find('[col-id="description"]').text()).to.equal(outcomeDescription)
        })
    })
})
