import 'cypress-if'

const specExtension = '.cy.js'

const filter = Cypress.$(parent.document.body).find('div#app').find('#inline-spec-list-header-search').val()

const specList = Cypress.$(parent.document.body)
  .find('div#app .specs-list-container ul:eq(0) li')
  .map((index, el) => {
    const text = el.innerText.replace('\n', '').replace('\\', '/')
    const path = Cypress.$(el).find('a').attr('href').split('?file=')[1]
    return {
      text,
      path,
    }
  })
  .filter((index, item) => {
    return item.text.endsWith(specExtension) && !item.text.startsWith('_')
  })
  .map((index, item) => item.path)
  .toArray()

const filterHasTag = (filter) => filter && filter.startsWith('@')

const generate = (specList, filter) => {
  const isTag = filterHasTag(filter)
  const indexSpecName = filter
    ? `_run-[${filter}]-${isTag ? 'tag' : 'filter'}${specExtension}`
    : `_run-all${specExtension}`
  const msg = `Processing ${isTag ? 'tag' : filter ? 'filter' : 'all'}: ${filter}`
  cy.log(msg)

  let content = `// generated script for specs filtered with "${filter}"\n\n`
  if (isTag) {
    content += `import cypressGrep from '@cypress/grep';\n`
    content += `Cypress.env('grepTags', '${filter}');\n`
    content += 'cypressGrep();\n\n'
  }
  content += specList
    .map((specPath) => {
      return `context('${specPath}', () => require('..${specPath.replace('cypress/e2e', '')}'))`
    })
    .join('\n')
  cy.writeFile(`./cypress/e2e/_generated-tests/${indexSpecName}`, content)
}

it('', () => {
  cy.wrap(filterHasTag(filter), { log: false })
    .if()
    .task('specsWithTag', { specs: specList, tag: '@smoke' })
    .then((tagged) => generate(tagged, filter))
    .else()
    .then(() => generate(specList, filter))
})
