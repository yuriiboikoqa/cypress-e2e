import { dataObject } from '../../fixtures/data/data-spec-projects'
import DiagramUtils from '../../support/DiagramUtils'
import TabPropertiesSideBar from '../../classes/bars/TabPropertiesSideBar'
import TabFlowSideBar from '../../classes/bars/TabFlowSideBar'

describe('Node Duplicate', () => {
  beforeEach(() => {
    cy.precondition('11978')
  })

  afterEach(() => {
    cy.removeProject(Cypress.env('projectId'))
  })

  it('[CMTS-11978] It is not possible to duplicate parent node with its child', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    TabPropertiesSideBar.isTitleSet('Tag1')
      .isNodeLabelSet('Tag1')
      .isNodeLabelEditBtnDisplayed()
      .clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- Tag1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Tag1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Tag1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
  })

  it('[CMTS-11979] Duplicated node has red frame', () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.tag1.nodeLabel)
    TabPropertiesSideBar.isTitleSet('Tag1')
      .isNodeLabelSet('Tag1')
      .isNodeLabelEditBtnDisplayed()
      .clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- Tag1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Tag1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Tag1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq1.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- Tag1', dataObject.flow_diagram.closedq2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
  })

  it('[CMTS-11980] Duplicated node has the same flows as original one', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
    TabPropertiesSideBar.openProperties()
      .isTitleSet('ClosedQuestion2')
      .isNodeLabelSet(dataObject.flow_diagram.closedq2.nodeLabel)

    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion3').then(($el) => {
      expect($el.data.key).equals('ClosedQuestion3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion4').then(($el) => {
      expect($el.data.key).equals('ClosedQuestion4')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'Repair').then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq2.nodeLabel)
    })

    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- ClosedQuestion2')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
      .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion3').then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion4').then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'Repair').then(($el) => {
      expect($el.data.key).equals('Copy1- ClosedQuestion2')
    })
  })

  it('[CMTS-11982] Copy - prefixes count the number of duplicates', () => {
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
    TabPropertiesSideBar.isTitleSet('ClosedQuestion2')
      .isNodeLabelSet('ClosedQuestion2')
      .clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- ClosedQuestion2')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
    DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy2- ClosedQuestion2')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('Label = Copy2- ClosedQuestion2')
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isToastMessageDisplayed('Duplicate Node "Copy2- ClosedQuestion2" has an invalid label and must be changed')
  })

  it('[CMTS-14485] Duplicated nodes line can be scrolled', () => {
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)

    DiagramUtils.findDiagram()
    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelEditBtnDisplayed()

    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- Prompt1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy2- Prompt1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy2- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy2- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy2- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode7')
    })
    DiagramUtils.findChildNodeByLinkText('Copy2- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode8')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('Copy3- Prompt1') // need to scroll to click on this node
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy3- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy3- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy3- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode9')
    })
    DiagramUtils.findChildNodeByLinkText('Copy3- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode10')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('Copy4- Prompt1') // need to scroll to click on this node
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy4- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy4- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy4- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode11')
    })
    DiagramUtils.findChildNodeByLinkText('Copy4- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode12')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('Copy5- Prompt1') // need to scroll to click on this node
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy5- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy5- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy5- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode13')
    })
    DiagramUtils.findChildNodeByLinkText('Copy5- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode14')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('Copy6- Prompt1') // need to scroll to click on this node
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy6- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy6- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy6- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode15')
    })
    DiagramUtils.findChildNodeByLinkText('Copy6- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode16')
    })

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.clickOnDuplicateBtn({ force: true }).clickOnCloseBtn()
    DiagramUtils.findDiagram()
    DiagramUtils.doubleClick('Copy7- Prompt1') // need to scroll to click on this node
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy7- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy7- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy7- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode17')
    })
    DiagramUtils.findChildNodeByLinkText('Copy7- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode18')
    })

    TabPropertiesSideBar.clickOnCloseBtn()
  })

  it('[CMTS-14486] Duplicated node can be renamed', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelEditBtnDisplayed()
      .clickOnDuplicateBtn({ force: true })

    DiagramUtils.click('Copy1- Prompt1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })

    TabPropertiesSideBar.setNodeLabel('editedcopy').isNodeLabelSet('editedcopy')
  })

  it('[CMTS-14487] The description can be added to the Duplicated node', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelSet(dataObject.flow_diagram.prompt1.nodeLabel)
      .isNodeLabelEditBtnDisplayed()
      .clickOnDuplicateBtn({ force: true })

    DiagramUtils.click('Copy1- Prompt1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')

    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', 'default').then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- Prompt1', dataObject.flow_diagram.tag2.linkLabel).then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })

    TabPropertiesSideBar.setNodeLabel('editedcopy')
      .isNodeLabelSet('editedcopy')
      .setDescription('this is description for duplicated node')
      .isDescriptionSet('this is description for duplicated node')
  })

  it('[CMTS-14488] Duplicated node is not attached to the flow (nodes tree)', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.prompt1.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.prompt1.nodeLabel).isNodeLabelSet(
      dataObject.flow_diagram.prompt1.nodeLabel,
    )
    TabFlowSideBar.openFlowTab()

    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.prompt1.nodeLabel, 'default').then(($el) => {
      expect($el.data.key).equals('endpointNode2')
    })
    DiagramUtils.findChildNodeByLinkText(
      dataObject.flow_diagram.prompt1.nodeLabel,
      dataObject.flow_diagram.tag2.linkLabel,
    ).then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.tag2.nodeLabel)
    })

    TabPropertiesSideBar.openProperties().clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- Prompt1')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- Prompt1')
      .isToastMessageDisplayed('Duplicate Node "Copy1- Prompt1" has an invalid label and must be changed')
  })

  it('[CMTS-14489] Duplicated node flows have Addpoints in copied flows', { tags: '@smoke' }, () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.closedq2.nodeLabel).isNodeLabelSet(
      dataObject.flow_diagram.closedq2.nodeLabel,
    )
    TabFlowSideBar.openFlowTab()

    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'Repair').then(($el) => {
      expect($el.data.key).equals(dataObject.flow_diagram.closedq2.nodeLabel)
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion3').then(($el) => {
      expect($el.data.key).equals('ClosedQuestion3')
    })
    DiagramUtils.findChildNodeByLinkText(dataObject.flow_diagram.closedq2.nodeLabel, 'closedquestion4').then(($el) => {
      expect($el.data.key).equals('ClosedQuestion4')
    })

    TabPropertiesSideBar.openProperties().clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- ClosedQuestion2')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
      .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
    TabFlowSideBar.openFlowTab()

    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'Repair').then(($el) => {
      expect($el.data.key).equals('Copy1- ClosedQuestion2')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion3').then(($el) => {
      expect($el.data.key).equals('AddNode5')
    })
    DiagramUtils.findChildNodeByLinkText('Copy1- ClosedQuestion2', 'closedquestion4').then(($el) => {
      expect($el.data.key).equals('AddNode6')
    })
  })

  it('[CMTS-14490] Duplicated node flow diagram displayed works the same as for original node', () => {
    DiagramUtils.findDiagram()

    DiagramUtils.doubleClick(dataObject.pattern_diagram.subflow1.nodeLabel)
    DiagramUtils.findDiagram()

    DiagramUtils.click(dataObject.flow_diagram.closedq2.nodeLabel)
    TabPropertiesSideBar.isTitleSet(dataObject.flow_diagram.closedq2.nodeLabel)
      .isNodeLabelSet(dataObject.flow_diagram.closedq2.nodeLabel)
      .clickOnDuplicateBtn({ force: true })
    DiagramUtils.click('Copy1- ClosedQuestion2')
    TabPropertiesSideBar.clickOnDatamodelListBtn()
      .isDatamodelHumanParameterSet('isNotConnected = true')
      .isDatamodelHumanParameterSet('Label = Copy1- ClosedQuestion2')
      .isToastMessageDisplayed('Duplicate Node "Copy1- ClosedQuestion2" has an invalid label and must be changed')
      .isDescriptionBtnFocused()
      .clickOnOutputBtn()
      .clickOnBothBtn()
  })
})
