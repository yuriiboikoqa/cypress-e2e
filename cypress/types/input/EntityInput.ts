export interface UserInput {
  entityLabel: string
  enitityDescription: string
  type: string
  email: string
  formatType: string
  mockValue: string
  PII: boolean
  Writable: boolean
  entityGroup: entityGroup[]
}

export interface entityGroup {
  label: string
  description: string
  primaryKey: string
  dependsOn: Array<string>
  sharedEndpoint: boolean
}
