import {GeneralTabSideBar} from './GeneralTabSideBar'

class TabChannelsSideBar extends GeneralTabSideBar {
  constructor(
    private channelsSel = () => cy.get('li[aria-controls="channelTab"]'),
    private currentChannelSel = () => cy.xpath('(//div[@class="itemHeader"])[2]'),
    private checkChanneNameSel = (linkNode: string) =>
      cy.xpath(
        `//div[@class="itemHeader" and contains(text(), "${linkNode}")]/following-sibling::div[@class="contentBody"]`,
      ),
    private editButtonSel = (text: string) =>
      cy.xpath(`//div[@class="itemHeader" and contains(text(), "${text}")]//button`),
    private nameNodeSel = (linkNode: string) =>
      cy.xpath('//div[@class="itemHeader" and contains(text(), "' + linkNode + '")]'),
    private removeChannelBtn = '//li[contains(text(), "Remove Channel")]',
    private messageTextSel = '//div[text()="This channel has items in its flowData. Are you sure you want to delete?"]',
    private yesButtonSel = '//button[@class="ant-btn ant-btn-primary ant-btn-sm"]',
  ) {
    super()
  }

  openChannels() {
    this.channelsSel().should('be.visible').click()

    return this
  }

  isChannelChosen(text: string) {
    this.currentChannelSel().should('have.text', text)

    return this
  }

  isCheckChannelName(text: string) {
    this.checkChanneNameSel(text).should('contain.text', text)

    return this
  }

  isNotExistChannelName(linkNode: string, text: string) {
    this.checkChanneNameSel(linkNode).should('not.exist', text)

    return this
  }

  clickEditBtn(text: string, opt: Partial<Cypress.TypeOptions> = {}) {
    opt.force = true
    this.editButtonSel(text).should('be.visible').click({ multiple: true })

    return this
  }

  isSideNode(linkNode: string, text: string) {
    this.nameNodeSel(linkNode).should('contain.text', text)

    return this
  }

  clickRemoveButton(opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.removeChannelBtn).should('be.visible').click(opt)

    return this
  }

  checkTextMessage() {
    cy.xpath(this.messageTextSel).should('be.visible')

    return this
  }

  clickYesButton(opt?: Partial<Cypress.TypeOptions>) {
    cy.xpath(this.yesButtonSel).should('be.visible').click(opt)

    return this
  }
}

export default new TabChannelsSideBar()
