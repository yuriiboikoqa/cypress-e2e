declare global {
  namespace Cypress {
    interface Chainable {
      deleteUser: typeof deleteUser
    }
  }
}

export default function deleteUser() {
  cy.request({
    method: 'DELETE',
    url: Cypress.env('baseUrlAPI') + '/admin/users/' + Cypress.env('usernameDel'),
    headers: {
      Authorization: 'Bearer ' + Cypress.env('token'),
    },
  }).then((resp) => {
    expect(resp.body.status).eql('success')
    expect(resp.body.code).eql(200)
    expect(resp.body.message).eql('User successfully deleted')
  })
}
