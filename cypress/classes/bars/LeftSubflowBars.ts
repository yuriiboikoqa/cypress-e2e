export class LeftSubflowBars {
  constructor(
    protected checkTextExits = () => cy.xpath('//span[@class="circleLabel"]'),
    protected checkTitleExit = (text: string) =>
      cy.xpath('//div[@class="paletteLabel" and contains(text(), "' + text + '")]'),
    protected subflowDefaultColorSelecor = () => cy.get('#exitDiv > div:nth-of-type(2) > div:first-of-type'),
    protected subflowEscalateColorSelecor = () => cy.get('#exitDiv > div:nth-of-type(3) > div:first-of-type'),
    protected subflowAbandonmentColorSelecor = () => cy.get('#exitDiv > div:nth-of-type(4) > div:first-of-type'),
  ) {}

  isCheckExitByText(textExit: string) {
    this.checkTextExits()
      .filter(':contains("' + textExit + '")')
      .should('be.visible')

    return this
  }

  checkTitleSubflow(text: string) {
    this.checkTitleExit(text).should('contain.text', text)

    return this
  }

  checkDefaultExitColor(checkBorder: string) {
    this.subflowDefaultColorSelecor().should('have.css', 'border', checkBorder)

    return this
  }

  checkEscalateExitColor(checkBorder: string) {
    this.subflowEscalateColorSelecor().should('have.css', 'border', checkBorder)

    return this
  }

  checkAbandonmentExitColor(checkBorder: string) {
    this.subflowAbandonmentColorSelecor().should('have.css', 'border', checkBorder)

    return this
  }
}

export default new LeftSubflowBars()
