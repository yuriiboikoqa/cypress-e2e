import { messages } from '../messagesErrorText'

// eslint-disable-next-line import/prefer-default-export
const NodeLabel: { title: string; input: string; message?: string; error: boolean }[] = [
  {
    title: '1 char',
    input: 'a',
    message: messages.characterLength,
    error: true,
  },
  {
    title: 'exisiting label',
    input: 'nodeLabel',
    message: messages.existingNodeLabel,
    error: true,
  },
  {
    title: '33 characters',
    input: 'morethan32morethan32morethan32mor',
    message: messages.labelLength,
    error: true,
  },
  {
    title: 'space in name',
    input: 'space in it',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: 'undersore_in_label',
    input: 'undersore_in_label',
    message: messages.spacesAndUnderscores,
    error: true,
  },
  {
    title: '2 chars',
    input: 'aa',
    error: false,
  },
  {
    title: '32 chars',
    input: 'longnamelongnamelongnamelongname',
    error: false,
  },
  {
    title: 'special symbols',
    input: 'label"with&special//chars',
    message: messages.startWithLetter,
    error: true,
  },
  {
    title: 'digits',
    input: '12345',
    message: messages.startWithLetter,
    error: true,
  },
  {
    title: 'empty',
    input: '',
    message: messages.uniqueNodeLabel,
    error: true,
  },
]
export default NodeLabel
