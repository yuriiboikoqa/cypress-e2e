let labelWithReasonFallback: {
  title: string
  linkLabel: string
  linkType: string
  preconditionId: string
  reason: string
  sideText: string
  conditionText: string
}[] = [
  {
    title: '[CMTS-13984] Global Fallback outcome can be added without a reason',
    preconditionId: '13984',
    linkLabel: 'down',
    linkType: ' Normal (down) ',
    reason: '',
    sideText: 'down',
    conditionText: 'Fallback',
  },
  {
    title: '[CMTS-13983] Global Fallback outcome reason can be added via the Add Outcome pop-up',
    preconditionId: '13983',
    linkLabel: 'ab_@#%^&',
    linkType: ' Normal (down) ',
    reason: 'Reason for escalation',
    sideText: 'down',
    conditionText: 'Fallback',
  },
]

export default labelWithReasonFallback
