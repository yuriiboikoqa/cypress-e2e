export class DialogPopOver {
  constructor(
    protected popoverTitleSel = '.ant-popover-title',
    protected redHighlighted = '.ant-form-item-control.has-error',
    protected requiredField = '.ant-form-item-required',
    protected validationMsg = '.ant-form-explain',
    protected plusButton = () => cy.get('.addButton'),
    protected popoverTitle = () => cy.get('.ant-popover-title'),
    protected popoverSel = '.ant-popover-content',
    protected addBtnSel = '//button[@class="ant-btn ant-btn-primary" and span[contains(text(), "Add")]]',
    protected saveBtnSel = () => cy.xpath('//button[./span[contains(text(), "Save")]]'),
    protected resetBtnSel = '//button[@class="ant-btn" and span[contains(text(), "Reset")]]',
  ) {}

  openPopOver() {
    this.plusButton().filter(':visible').should('be.visible').click()
  }

  isOpen() {
    cy.get(this.popoverSel).should('be.visible')

    return this
  }

  isClose() {
    cy.get(this.popoverSel).should('not.be.visible')

    return this
  }

  clickOnAddBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.addBtnSel).filter(':visible').click(opt)

    return this
  }

  clickOnSaveBtn(opt?: Partial<Cypress.ClickOptions>) {
    this.saveBtnSel().filter(':visible').click(opt)

    return this
  }

  clickOnResetBtn(opt?: Partial<Cypress.ClickOptions>) {
    cy.xpath(this.resetBtnSel).filter(':visible').click(opt)

    return this
  }

  checkValidationError(label: string, message: string, field: string, input: string, flag: boolean) {
    if (input.length > 0) {
      cy.get(field).filter(':visible').clear({ force: true }).type(input)
      cy.get(this.popoverTitleSel).filter(':visible').click()
    } else {
      cy.get(field).should('be.visible').clear()
      cy.get(this.popoverTitleSel).filter(':visible').click()
    }
    if (flag === true) {
      cy.get(label)
        .filter(':visible')
        .parent()
        .siblings()
        .find(this.validationMsg)
        .should('exist')
        .and('have.text', message)
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlighted).should('exist')
    } else {
      cy.get(label).filter(':visible').parent().siblings().find(this.validationMsg).should('not.exist')
      cy.get(label).filter(':visible').parent().siblings().find(this.redHighlighted).should('not.exist')
    }
  }
}

export default new DialogPopOver()
