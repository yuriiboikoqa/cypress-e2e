export class PaletteBox {
  constructor() {
    this.filterBarIdSelector = '#filterBar'
    // nodes selectors
    this.subflowNodeIdSelector = '#subflowNode'
    this.skillRouterNodeIdSelector = '#routerNode'
    this.tagNodeIdSelector = '#tagNode'
    this.goToNodeIdSelector = '#gotoNode'
    this.promptNodeIdSelector = '#promptNode'
    this.closedQNodeIdSelector = '#openNode' // looks we have minor bug here with naming
    this.openQNodeIdSelector = '#closeNode' // looks we have minor bug here with naming
    this.globalFallbackSelector = '#globalDiv > div:nth-child(3)' // selector for Global Fallback Outcome
    this.globalEscalateSelector = '#globalDiv > div:nth-child(2)' // selector for Global Escalate Outcome
    this.patternAbandonSelector = '#outcomeDiv > div:nth-child(2)' // selector for Pattern Abandon Outcome
    this.patternHITLSelector = '#outcomeDiv > div:nth-child(3)' // selector for Pattern HITL Outcome
    this.patternContainedSelector = '#outcomeDiv > div:nth-child(4)' // selector for Pattern Contained Outcome
    this.globalOutcomebackIdSelector = '#globalDiv > div:nth-child(2)'
    this.subflowDefaultIdSelecor = '#exitDiv > div:nth-of-type(2)'
    this.subflowEscalateIdSelector = '#exitDiv > div:nth-of-type(3)'
    this.subflowAbandonmentIdSelecor = '#exitDiv > div:nth-of-type(4)'
    this.exitsExpandCollapse = '.ant-collapse-header'
    this.exitDiv = '#exitDiv'
    this.circleLabel = '.circleLabel'
    this.exitsSection = '#filterBar > div > div:nth-child(2)'
  }

  expandCollapseExits() {
    return cy.contains('[role="button"]', 'Exits').then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  checkExitColor(name) {
    return cy
      .contains(this.circleLabel, name)
      .parent()
      .prev()
      .then(($element) => {
        cy.wrap($element).then(($wrapped) => {
          $wrapped
        })
      })
  }

  element() {
    return cy.get(this.filterBarIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  subflow() {
    return cy.get(this.subflowNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  skillsrouter() {
    return cy.get(this.skillRouterNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  tag() {
    return cy.get(this.tagNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  goto() {
    return cy.get(this.goToNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  prompt() {
    return cy.get(this.promptNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  openq() {
    return cy.get(this.openQNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  closedq() {
    return cy.get(this.closedQNodeIdSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  escalate() {
    return cy.get(this.globalEscalateSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  fallback() {
    return cy.get(this.globalFallbackSelector).then(($element) => {
      cy.wrap($element).then(($wrapped) => {
        $wrapped
      })
    })
  }

  checkPatternOutcome(outcome) {
    cy.get(this.circleLabel).contains(outcome).should('exist')
  }

  checkSubNodeLabel(nodeName, updatedLabel) {
    cy.contains(this.exitDiv, nodeName).find(this.circleLabel).should('contain', updatedLabel)
  }

  expand(collapseText) {
    cy.get(this.exitsExpandCollapse).contains(collapseText).should('be.visible').click()
    cy.get(this.exitsExpandCollapse)
      .contains(collapseText)
      .then(($collapse) => {
        cy.wrap($collapse).should('have.attr', 'aria-expanded', 'true')
      })
  }

  collapse(collapseText) {
    cy.get(this.exitsExpandCollapse).contains(collapseText).contains(collapseText).should('be.visible').click()
    cy.get(this.exitsExpandCollapse)
      .contains(collapseText)
      .then(($collapse) => {
        cy.wrap($collapse).should('not.have.attr', 'aria-expanded')
      })
  }
}

export default new PaletteBox()
