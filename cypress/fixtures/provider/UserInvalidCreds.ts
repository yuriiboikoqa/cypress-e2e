let userInvalidCreds: {
  title: string
  username: string
  password: string
  errors: { status: string; message: string }
}[] = [
  {
    title: '[CMTS-11010] Login page. Check login with incorrect username and password (API)',
    username: 'db-admin',
    password: 'db@dm!n123',
    errors: {
      status: 'failed',
      message: 'unable to login user db-admin: Error: Request failed with status code 401',
    },
  },
  {
    title: '[CMTS-11008] Login page. Check login with incorrect username (API)',
    username: 'db-admin',
    password: Cypress.env('password'),
    errors: {
      status: 'failed',
      message: 'unable to login user db-admin: Error: Request failed with status code 401',
    },
  },
  {
    title: '[CMTS-11009] Login page. Check login with incorrect password (API)',
    username: Cypress.env('username'),
    password: 'db@dm!n123',
    errors: {
      status: 'failed',
      message: 'unable to login user ' + Cypress.env('username') + ': Error: Request failed with status code 401',
    },
  },
  {
    title: '[CMTS-11007] Login page. Check login with reverse username and password credentials (API)',
    username: Cypress.env('password'),
    password: Cypress.env('username'),
    errors: {
      status: 'failed',
      message: 'unable to login user ' + Cypress.env('password') + ': Error: Request failed with status code 401',
    },
  },
  {
    title: 'login with username spaces and correct password',
    username: '         ',
    password: Cypress.env('password'),
    errors: {
      status: 'failed',
      message: 'unable to login user          : Error: Request failed with status code 401',
    },
  },
  {
    title: 'login with correct username and another password register',
    username: Cypress.env('username'),
    password: Cypress.env<string>('password').toUpperCase(),
    errors: {
      status: 'failed',
      message: 'unable to login user ' + Cypress.env('username') + ': Error: Request failed with status code 401',
    },
  },
  {
    title: '[CMTS-11011] Login page. Check login with xss and correct password (API)',
    username: '<script>alert(123)</script>',
    password: 'dhb-admin',
    errors: {
      status: 'failed',
      message: 'unable to login user <script>alert(123)</script>: Error: Request failed with status code 401',
    },
  },
  {
    title: '[CMTS-11014] Login page. Check login with credentials of deleted user (API)',
    username: 'admin',
    password: 'dhb-admin',
    errors: {
      status: 'failed',
      message: 'unable to login user admin: Error: Request failed with status code 401',
    },
  },
]

export default userInvalidCreds
