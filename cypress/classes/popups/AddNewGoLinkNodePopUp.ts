import { AddNewNodePopUp } from './AddNewNodePopUp'

export class AddNewGoLinkNodePopUp extends AddNewNodePopUp {
  constructor(
    protected linkTypeSel = () => cy.xpath('(//div[@class="ant-select ant-select-enabled"])[1]'),
    protected chooseLinkTypeSel = () => cy.get('li[role="option"]'),
    protected chooseLinkToNodeSel = () => cy.get('li[role="option"]'),
    protected linkToNodeSel = () => cy.xpath('//label[@title="Link to Node"]//..//..//span[@class="ant-select-arrow"]'),
    protected errorMessageLinkToNodeSel = () =>
      cy.xpath('//div[@class="ant-form-explain" and contains(text(), "Must select a node to link to.")]'),
    protected emptyLinkToNodeSel = () =>
      cy.xpath('(//div[@class="ant-select-selection ant-select-selection--single"])[2]'),
  ) {
    super()
  }

  selectLinkType(text: string) {
    this.linkTypeSel().should('be.visible').click()
    this.chooseLinkTypeSel().contains(text).click()

    return this
  }

  isLinkTypeSet(text: string) {
    this.linkTypeSel().should('contain.text', text)

    return this
  }

  selectLinkToNode(text: string) {
    this.linkToNodeSel().should('be.visible').click()
    this.chooseLinkToNodeSel().contains(text).click()

    return this
  }

  isLinkToNodeSet(text: string) {
    this.emptyLinkToNodeSel().should('contain.text', text)

    return this
  }

  isLinkToNodeErrorMessageNotDisplayed() {
    this.errorMessageLinkToNodeSel().should('not.exist')

    return this
  }

  isLinkToNodeErrorMessageDisplayed() {
    this.errorMessageLinkToNodeSel().should('exist')

    return this
  }
}

export default new AddNewGoLinkNodePopUp()
